<?php
/**
* Library "Often Used Functions"
* 
* In this library collected functions on PHP often used on web-sites
*
* @version 1.14
* @copyright Copyright (C) 2003 - 2018 Richter
* @author Richter (richter@wpdom.com)
* @link http://wpdom.com
*/

namespace Ardzo;

/** Validate an e-mail address
* @param string $email one address or array
*/
function validateEmail($email)
{
    if (!is_array($email)) { $tmp = $email; unset($email); $email[0] = $tmp; }
    foreach ($email as $val) {
        if (!(preg_match("/^[a-z0-9._-]+@[a-z0-9._-]+\.[a-z]{2,4}$/i", $val))) return FALSE;
    }
    return TRUE;
}

/**
* Return all values of specified tag from XML-fragment
* @return array
*/
function getFa($text, $tag)
{
    preg_match_all("/<$tag>(.*?)<\/$tag>/s", $text, $out);
    return $out[1];
}

/** Return first value of specified tag from XML-fragment */
function getF($text,$tag)
{
    $out = getFa($text, $tag);
    return $out[0];
}

/** Return IP-address of user */
function getIp()
{
    $ip = getenv('HTTP_X_FORWARDED_FOR');
    if (!$ip) {
        $ip = getenv('REMOTE_ADDR');
    } else {
        $tmp = ",";
        if (strlen(strstr($ip, $tmp)) != 0) {
            $ips = explode($tmp, $ip);
            $ip = $ips[count($ips) - 1];
        }
    }
    return trim($ip);
}

/**
* Return Whois-information
* Uses database of RIPE NCC
* @param string $address can be IP-address or hostname
*/
function getWhois($address)
{
    $res = '';
    if (empty($address)) return 'No search key specified';
    $socket = fsockopen("whois.ripe.net", 43, $errno, $errstr);
    if (!$socket) {
        return $errstr($errno);
    } else {
        fputs($socket, $address."\r\n");
        while (!feof($socket)) {
            $res .= fgets($socket, 128);
        }
    }
    fclose ($socket);
    return $res;
}

/** Return cut string
* @param string $in_str string
* @param int $len required length
* @return string
*/
function iSubStr($in_str, $len)
{
    $tmp_1 = mb_substr($in_str, 0, $len);
    if (mb_strlen($tmp_1) == $len) { // Scrap of incomplete words
        for (;;) {
            if (mb_substr($tmp_1, -1) == ' ')
                break;
            else
                $tmp_1 = mb_substr($tmp_1, 0, -1);
        }
    }
    return $tmp_1;
}

/** Paging type 1 (numbers of a pages) */
function getPaging1($item_q, $item_on_page, $cur_page, $tmpl_cur_page, $tmpl_oth_page, $divider = '', $max_simple = 30, $block_first = '|&lt;', $block_last = '&gt;|', $block_prev = '&lt;&lt;', $block_next = '&gt;&gt;')
{
    $paging = '';
    if (!is_numeric($cur_page)) $cur_page = 1;
    if ($item_on_page == 0) $item_on_page = 1;

    $max_page = ceil($item_q / $item_on_page);

    if ($max_page <= $max_simple) { // Simple
        for ($I=1; $I<=$max_page; $I++) {
            if ($I == $cur_page) $paging .= str_replace("[NUM]",$I, $tmpl_cur_page);
            else {
                $page_no = $page_show = $I;
                eval($tmpl_oth_page);
                $paging .= $page_num;
            }
            if ($I < $max_page) $paging .= $divider;
        }
    } else { // Complex
        if ($cur_page > 1) {
            $page_no = 1;
            $page_show = $block_first;
            eval($tmpl_oth_page);
            $paging .= $page_num;
            $page_no = $cur_page - 1;
            $page_show = $block_prev;
            eval($tmpl_oth_page);
            $paging .= $page_num;
        }
        if ($cur_page > 3) $paging .= '... ';
        for ($I=$cur_page-2; $I<=$cur_page+2; $I++) {
            if ($I>0 and $I<=$max_page) {
                if ($I == $cur_page) $paging .= str_replace("[NUM]", $I, $tmpl_cur_page);
                else {
                    $page_no = $page_show = $I;
                    eval($tmpl_oth_page);
                    $paging .= $page_num;
                }
                if ($I<$cur_page+2 and $I<$max_page) $paging .= $divider;
            }
        }
        if ($cur_page < $max_page-2) $paging .= '... ';
        if ($cur_page < $max_page) {
            $page_no = $cur_page + 1;
            $page_show = $block_next;
            eval($tmpl_oth_page);
            $paging .= $page_num;
            $page_no = $max_page;
            $page_show = $block_last;
            eval($tmpl_oth_page);
            $paging .= $page_num;
        }
    }

    return $paging;
}

/** Paging type 2 (numbers of a items, divided on groups) */
function getPaging2($item_q, $item_on_page, $cur_page, $tmpl_cur_page, $tmpl_oth_page)
{
    $paging = '';
    if (!is_numeric($cur_page)) $cur_page = 1;

    $page_no = 0;
    for ($I=1; $I<=$item_q; $I=$I+$item_on_page) {
        $page_no++;
        $num_block = $I;
        $I1 = $I + $item_on_page - 1;
        if ($I1<=$item_q && $I!=$I1) $num_block .= '-'.$I1;
        if ($page_no != $cur_page) {
            eval($tmpl_oth_page);
            $paging .= $page_num;
        } else {
            $paging .= str_replace("[NUM]", $num_block, $tmpl_cur_page);
        }
    }
    return $paging;
}

/** Reads a content of ini-file
* @param boolean $ignore_sections ignore sections ("[Something]")
*/
function readIni($file_name, $ignore_sections = FALSE)
{
    $dump = file($file_name);
    foreach ($dump as $val) {
        if (trim($val) != '') {
            if (substr($val, 0, 1) == '[') {
                $first_index = trim(substr($val, 1, strlen($val)-4));
            } else {
                preg_match ("/(.*?)=/", $val, $tmp_index);
                preg_match ("/=(.*?)$/", $val, $tmp_value);
                if ($ignore_sections) $out[$tmp_index[1]] = $tmp_value[1];
                else $out[@$first_index][@$tmp_index[1]] = @$tmp_value[1];
            }
        }
    }
    return($out);
}

/** Deletes all files in specified folder
* @param boolean $delete_folder TRUE - delete same folder also
*/
function deleteFiles($folder, $delete_folder = TRUE)
{
    if (substr($folder,-1) != '/') $folder .= '/';
    $out['file_deleted'] = $out['folder_deleted'] = 0;

    if ($dir = @opendir($folder)) { 
        while (($file = readdir($dir)) !== false) {
            if ($file!='.' && $file!='..' && filetype($folder.$file)=='file') {
                unlink($folder.$file);
                $out['file_deleted']++;
            } elseif ($file!='.' && $file!='..' && filetype($folder.$file)=='dir') {
                $tmp = deleteFiles($folder.$file.'/');
                $out['file_deleted'] += $tmp['file_deleted'];
                $out['folder_deleted'] += $tmp['folder_deleted'];
            }
        }
        closedir($dir);
        if ($delete_folder) {
            rmdir(substr($folder, 0, strlen($folder)-1));
            $out['folder_deleted']++;
        }
    }
    return $out;
}

/** Calculates a quantity of files and folders in specified folder */
function countFiles($folder)
{
    if (substr($folder,-1) != '/') $folder .= '/';
    $q['files'] = $q['files_size'] = $q['folders'] = 0;

    if ($dir = @opendir($folder)) { 
        while (($file = readdir($dir)) !== false) {
            if ($file!='.' && $file!='..' && filetype($folder.$file)=='file') {
                $q['files']++;
                $q['files_size'] += filesize($folder.$file);
            } elseif ($file!='.' && $file!='..' && filetype($folder.$file)=='dir') {
                $q['folders']++;
                $tmp = countFiles($folder.$file.'/');
                $q['files'] += $tmp['files'];
                $q['files_size'] += $tmp['files_size'];
                $q['folders'] += $tmp['folders'];
            }
        }
        closedir($dir);
    }
    return $q;
}

/** Return "breadcrumbs"
* @source $breadcrumbs = array('soft.php'=>'Software', 'cms/index.php'=>'Ardzo.CMS', ''=>'Order');
*/
function getBreadcrumbs($breadcrumbs, $divider = ' &rarr; ', $start_block = '<div class="breadcrumbs"><a href="index.php">Main page</a>', $end_block = '</div>')
{
    if (!empty($breadcrumbs)) {
        $tmp = $start_block;
        if (!is_array($breadcrumbs)) $breadcrumbs = array(''=>$breadcrumbs);
        foreach ($breadcrumbs as $url=>$title)
            if (empty($url)) $tmp .= $divider.$title;
            else $tmp .= $divider.'<a href="'.$url.'">'.$title.'</a>';
        $tmp .= $end_block;
        return $tmp;
    }
}

/** Split file name onto name and extension
* Extension returns with "."
*/
function splitFilename($file_name)
{
    preg_match("/\.([^\.]*?)$/", $file_name, $ext);
    $ext = strtolower($ext[0]);
    return array(substr($file_name, 0, strlen($file_name)-strlen($ext)), $ext);
}

/** Makes an inscription on picture */
function imgMakeInscription($input_file, $output_file, $inscription_file, $x, $y, $jpeg_quality)
{
    if (function_exists('ImageCreateFromJPEG')) {
        if (!$jpeg_quality) $jpeg_quality = 70;

        $tmp = GetImageSize($input_file);
        if (in_array($tmp[2], array(1,2,3))) {
            switch ($tmp[2]) {
                case 1: $img = ImageCreateFromGIF($input_file); break;
                case 2: $img = ImageCreateFromJPEG($input_file); break;
                case 3: $img = ImageCreateFromPNG($input_file); break;
            }
            $inscription = ImageCreateFromPNG($inscription_file);
            $tmp_i = GetImageSize($inscription_file);

            // Coordinates
            if (empty($x)) {
                $inscription_x = (ImageSX($img) - ImageSX($inscription))/2;
            } else {
                if ($x > 0) $inscription_x = $x; else $inscription_x = ImageSX($img) - ImageSX($inscription) + $x;
            }
            if (empty($y)) {
                $inscription_y = (ImageSY($img) - ImageSY($inscription))/2;
            } else {
                if ($y > 0) $inscription_y = $y; else $inscription_y = ImageSY($img) - ImageSY($inscription) + $y;
            }

            ImageCopy($img, $inscription, $inscription_x, $inscription_y, 0, 0, $tmp_i[0], $tmp_i[1]);
            switch ($tmp[2]) {
                case 1: ImageGIF($img, $output_file); break;
                case 2: ImageJPEG($img, $output_file, $jpeg_quality); break;
                case 3: ImagePNG($img, $output_file); break;
            }
        }
    }
}
?>
