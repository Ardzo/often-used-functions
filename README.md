# Library "Often Used Functions"

## Introduction
Library contains a functions on PHP often used on web-sites.

## Requirements
* OS MS Windows or Unix-like
* Apache 1.3.12+
* PHP 5.0+

## Using
Copy `"lib_ouf.php"` on server and use it in your script.  
Or use Composer - class will loads automatically.  
See example code in folder `"example"`.
In this folder there is files that you can use for fast testing of library.
