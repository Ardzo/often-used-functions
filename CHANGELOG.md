# Version history:

## **1.14** 2018-10-14
* All functions renamed
* Namespace is added
* In function `"validateEmail()"` replaced deprecated `"eregi()"`
* Function `"iSubStr()"` don't return "..." at the and of string
* Functions `"authenticate()"`, `"explodeDate()"` is removed
* Function `"deleteFiles()"` little improved

## **1.13** 2011-01-10
* Function `"get_paging1()"`: jumps to previous/next pages is added

## **1.12** 2010-10-04
* New function `"count_files()"`
* Function `"delete_files()"` is improved

## **1.11** 2009-05-09
* New functions `"splt_filename()"`, `"make_inscription()"`
* Function `"get_paging1()"`: divizion by zero is prevented
* Function `"validate_email()"`: fixed a detecting of dot
* Function `"validate_email()"` can take an array

## **1.10** 2008-11-21
* Function `"ISubStr()"` is multibyte ready

## **1.9** 2008-07-08
* Function `"Get_IP()"` is little improved

## **1.8** 2008-06-06
* Function `"get_breadcrumbs"` is added
* Function `"get_paging1()"` modified

## **1.7** 2008-05-21
* Function `"delete_files()"` is added

## **1.6** 2006-06-15
* Function `"get_paging1()"` modified

## **1.5** 2006-06-02
* Function `"get_fa()"`: modifier "s" is added
* Function `"get_paging1()"` modified

## **1.4** 2006-03-30
* Function `"read_ini()"` is added

## **1.3** 2004-05-17
* Function `"Get_Whois()"` is added

## **1.2** 2004-04-01
* Functions `"authenticate()"`, `"explode_date()"` is added

## **1.1** 2004-03-19
* Functions `"get_paging1()"`, `"get_paging2()"` is added

## **1.0** 2003-11-24
* This is first release
